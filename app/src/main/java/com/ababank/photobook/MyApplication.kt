package com.ababank.photobook

import android.app.Application
import android.util.Log
import com.ababank.photobook.di.UserManager
import com.ababank.photobook.di.component.DaggerAppComponent
import javax.inject.Inject

/**
 * @author Rithy
 * created at 5/17/2019
 */

class MyApplication : Application() {

    val appComponent by lazy {
        DaggerAppComponent.builder().appContext(this).build()
    }

    @Inject
    lateinit var userManager: UserManager

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        Log.d(">>>>", userManager.token)
    }
}