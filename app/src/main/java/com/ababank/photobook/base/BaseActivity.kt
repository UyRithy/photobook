package com.ababank.photobook.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.ababank.photobook.MyApplication

/**
 * @author Rithy
 * created at 5/17/2019
 */

abstract class BaseActivity<VM : BaseViewModel>(target: Class<VM>) : AppCompatActivity(), View {

    val app get() = (application as MyApplication)

    val viewModel : VM by lazy { ViewModelProviders.of(this)[target] }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.startActivity = { activityClass -> startActivity(activityClass) }
        viewModel.onAttach(app.appComponent)
        lifecycle.addObserver(viewModel)
    }


    open fun startActivity(targetClass: Class<out AppCompatActivity>) {
        val intent = Intent(this, targetClass)
        startActivity(intent)
    }

}