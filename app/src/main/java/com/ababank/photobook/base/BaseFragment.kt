package com.ababank.photobook.base

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateOvershootInterpolator
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.ababank.photobook.MyApplication


/**
 * @author Rithy
 * created at 5/18/2019
 */

abstract class BaseFragment<VM : BaseViewModel>(target: Class<VM>) : Fragment() {

    val viewModel: VM by lazy { ViewModelProviders.of(this)[target] }
    abstract val layoutResource: Int @LayoutRes get

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }

    private fun bind() {
        (activity?.application as MyApplication?)?.let { viewModel.onAttach(it.appComponent) }
        viewModel.startActivity = this::startActivity
        lifecycle.addObserver(viewModel)
    }

    open fun startActivity(target: Class<out AppCompatActivity>) {
        val intent = Intent(activity, target)
        this.startActivity(intent)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): android.view.View? {
        return inflater.inflate(layoutResource, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val animator = AnimatorSet()
        val translateY = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, 200f, 0f)
        val alpha = ObjectAnimator.ofFloat(view, View.ALPHA, 0.2f, 1f)
        animator.duration = 400
        animator.playTogether(translateY, alpha)
        animator.start()
    }
}