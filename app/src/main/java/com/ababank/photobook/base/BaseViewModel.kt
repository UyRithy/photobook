package com.ababank.photobook.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.ababank.photobook.di.component.AppComponent

/**
 * @author Rithy
 * created at 5/18/2019
 */

abstract class BaseViewModel : ViewModel(), LifecycleObserver {

    var startActivity: ((Class<out AppCompatActivity>) -> Unit)? = null

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    open fun onCreate() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun onResume() {
    }

    abstract fun onAttach(appComponent: AppComponent)
}