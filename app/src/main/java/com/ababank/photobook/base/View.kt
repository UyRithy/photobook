package com.ababank.photobook.base

import androidx.annotation.LayoutRes


/**
 * @author Rithy
 * created at 5/17/2019
 */

interface View {

    val layoutResource: Int @LayoutRes get
}