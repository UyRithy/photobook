package com.ababank.photobook.commons

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * @author Rithy
 * created at 5/18/2019
 */

abstract class BaseViewHolder<T>(view: View) : RecyclerView.ViewHolder(view) {

    protected var item: T? = null

    open fun onBind(item: T) {
        this.item = item
    }
}