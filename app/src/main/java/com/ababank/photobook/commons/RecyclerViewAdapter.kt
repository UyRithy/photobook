package com.ababank.photobook.commons

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


/**
 * @author Rithy
 * created at 5/18/2019
 */
class RecyclerViewAdapter <T, VH: BaseViewHolder<T>>
    constructor(private val items: ArrayList<T>,
                private val viewHolderProviders: ViewHolderProviders<T,VH>) : RecyclerView.Adapter<VH>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return viewHolderProviders.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemViewType(position: Int): Int {
      return viewHolderProviders.getItemType(position)
    }
}