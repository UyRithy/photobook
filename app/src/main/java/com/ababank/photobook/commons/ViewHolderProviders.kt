package com.ababank.photobook.commons

import android.view.ViewGroup

/**
 * @author Rithy
 * created at 5/18/2019
 */

interface ViewHolderProviders<T, VH : BaseViewHolder<T>> {

    fun onCreateViewHolder(container: ViewGroup, itemType: Int): VH

    fun getItemType(position: Int) = 0
}