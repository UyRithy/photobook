package com.ababank.photobook.commons.extention

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.onEndReached(callback: () -> Unit) {

    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            recyclerView.layoutManager?.let {
                if(it.itemCount -1 == it.getLastVisibility()) callback.invoke()
            }
        }
    })
}

private fun RecyclerView.LayoutManager.getLastVisibility() : Int {
    var position =0
    if(this is GridLayoutManager)
        position = this.findLastCompletelyVisibleItemPosition()
    else if(this is LinearLayoutManager)
        position = this.findLastCompletelyVisibleItemPosition()
    return position
}

