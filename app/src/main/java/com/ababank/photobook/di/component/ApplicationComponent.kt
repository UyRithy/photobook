package com.ababank.photobook.di.component

import com.ababank.photobook.MyApplication
import com.ababank.photobook.di.module.AppModule
import com.ababank.photobook.di.module.NetworkModule
import com.ababank.photobook.di.module.ViewModelModule
import com.ababank.photobook.ui.main.MainViewModel
import com.ababank.photobook.ui.main.bookmarks.BookmarkViewModel
import com.ababank.photobook.ui.main.photo.PhotoViewModel
import com.ababank.photobook.ui.main.photo.detail.PhotoDetailViewModel
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * @auth Rithy
 * created at 4/13/2019
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ViewModelModule::class,
    NetworkModule::class
])
interface AppComponent {

    fun inject(app: MyApplication)

    fun inject(viewModel: MainViewModel)

    fun inject(photoViewModel: PhotoViewModel)

    fun inject(photoDetailViewModel: PhotoDetailViewModel)

    fun inject(bookmarkViewModel: BookmarkViewModel)

    @Component.Builder
    interface Builder {
        @BindsInstance fun appContext(app: MyApplication) : Builder
        fun build() : AppComponent
    }
}