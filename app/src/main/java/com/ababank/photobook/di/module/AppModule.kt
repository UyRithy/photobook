package com.ababank.photobook.di.module

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ababank.photobook.MyApplication
import com.ababank.photobook.di.UserManager
import com.ababank.photobook.util.ConnectionUtils
import dagger.Module
import dagger.Provides
import javax.inject.Provider
import javax.inject.Singleton

/**
 * @author Rithy
 * created at 5/17/2019
 */

@Module
class AppModule {

    @Provides
    fun providesContext(app: MyApplication): Context = app

    @Singleton
    @Provides
    fun provideUserManager() = UserManager()

    @Singleton
    @Provides
    fun provideNetworkModue(app: MyApplication) = ConnectionUtils(app)
}