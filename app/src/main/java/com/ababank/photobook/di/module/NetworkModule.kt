package com.ababank.photobook.di.module

import com.ababank.photobook.BuildConfig
import com.ababank.photobook.services.AppServices
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
/**
 * @author Rithy
 * created at 5/18/2019
 */

@Module
class NetworkModule {

    @Provides
    fun providesHttpClient() : OkHttpClient {

        val client = OkHttpClient.Builder()
        if(BuildConfig.DEBUG) {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(logger)
        }
        client.connectTimeout(30,TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        return client.build()
    }

    @Provides
    fun provideRetrofit(httpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
                .baseUrl("https://picsum.photos/")
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    fun providesAppService(retrofit: Retrofit) : AppServices{
        return retrofit.create(AppServices::class.java)
    }
}