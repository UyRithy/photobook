package com.ababank.photobook.services

import com.ababank.photobook.services.response.model.Photo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface AppServices {

    @GET("v2/list")
    fun fetchPhotos(@Query("page") page: Int,
                    @Query("limit")limit: Int = 30) : Observable<List<Photo>>

}