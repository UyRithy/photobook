package com.ababank.photobook.services.response.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * @author Rithy
 * created at 5/18/2019
 */

data class Photo(
        @SerializedName("id")
        val id: Int,

        @SerializedName("author")
        val author: String,

        @SerializedName("width")
        val width: Int,

        @SerializedName("height")
        val height: Int,

        @SerializedName("url")
        val url: String,

        @SerializedName("download_url")
        val downloadUrl: String,

        var isBookmark: Boolean = false,

        var isSelected: Boolean = false
) : Serializable