package com.ababank.photobook.ui.main

import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.ababank.photobook.R
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.ui.main.about.AboutFragment
import com.ababank.photobook.ui.main.bookmarks.BookmarkFragment
import com.ababank.photobook.ui.main.photo.PhotosFragment
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainHost {

    private val icons = arrayListOf(
            R.drawable.ic_outline_insert_photo_24px,
            R.drawable.ic_outline_bookmarks_24px,
            R.drawable.outline_account_circle_24px)

    private val selectedIcons = arrayListOf(
            R.drawable.ic_baseline_photo_24px,
            R.drawable.ic_baseline_bookmarks_24px,
            R.drawable.baseline_account_circle_24px
    )

    private val fragments by lazy {
        arrayListOf(
                PhotosFragment.newInstance(this),
                BookmarkFragment(),
                AboutFragment()
        )
    }

    private val photosFragment by lazy { PhotosFragment.newInstance(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupTabLayout()
        val test = HashMap<Int, List<Photo>>()
        test[1] = arrayListOf()
        test[2] = arrayListOf()
        Log.d(">>>>", Gson().toJson(test))
    }

    private fun applyFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.mainContainer, fragment).commit()
    }

    private fun setupTabLayout() {
        tabLayout.getTabAt(0)?.let { changeTabStyle(it, true) }
        tabLayout.getTabAt(1)?.let { changeTabStyle(it, false) }
        tabLayout.getTabAt(2)?.let { changeTabStyle(it, false) }
        applyFragment(fragment = fragments[0] as Fragment)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.let { changeTabStyle(it, false) }
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    applyFragment(fragments[it.position] as Fragment)
                    changeTabStyle(it, true)
                }
            }
        })
    }

    fun changeTabStyle(tab: TabLayout.Tab, isSelected: Boolean) {
        val icon = if (isSelected) selectedIcons[tab.position] else icons[tab.position]
        val iconColor = ContextCompat.getColor(this, if (isSelected) R.color.colorPrimary else R.color.black_800)
        tab.setIcon(icon)
        tab.icon?.setColorFilter(iconColor, PorterDuff.Mode.SRC_IN)
    }
}
