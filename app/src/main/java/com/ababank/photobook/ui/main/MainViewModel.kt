package com.ababank.photobook.ui.main

import com.ababank.photobook.base.BaseViewModel
import com.ababank.photobook.di.component.AppComponent
/**
 * @author Rithy
 * created at 5/17/2019
 */

class MainViewModel: BaseViewModel() {

    override fun onAttach(appComponent: AppComponent) {
        appComponent.inject(this)
    }
}