package com.ababank.photobook.ui.main.about

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ababank.photobook.R
import kotlinx.android.synthetic.main.contact_layout.view.*
import kotlinx.android.synthetic.main.fragment_about.view.*
import kotlinx.android.synthetic.main.layout_author.*


/**
 * @author Rithy
 * created at 5/21/2019
 */
class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appCompatImageView.clipToOutline = true
        view.shareButton.setOnClickListener { shareProfile() }
        view.phoneNumberText.setOnClickListener { openDialer() }
        view.emailText.setOnClickListener { openEmail() }
    }

    private fun openEmail() {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:rithyuy.developer@gmail.com"))
        startActivity(Intent.createChooser(emailIntent, "Send email"))
    }

    private fun openDialer() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:+85586889241")
        startActivity(intent)
    }

    private fun shareProfile() {
        val profileUrl = "https://www.linkedin.com/in/uy-rithy/"
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, profileUrl)
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }
}