package com.ababank.photobook.ui.main.bookmarks

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.ababank.photobook.R
import com.ababank.photobook.base.BaseFragment
import com.ababank.photobook.commons.RecyclerViewAdapter
import com.ababank.photobook.commons.ViewHolderProviders
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.ui.main.photo.PhotoViewHolder
import com.ababank.photobook.ui.main.photo.detail.PhotoDetailActivity
import kotlinx.android.synthetic.main.fragment_bookmarks.*
import kotlinx.android.synthetic.main.fragment_bookmarks.view.*
import kotlinx.android.synthetic.main.item_photo.view.*
import kotlinx.android.synthetic.main.selection_tool_bar.*
import java.util.ArrayList

/**
 * @author Rithy
 * created at 5/18/2019
 */
class BookmarkFragment : BaseFragment<BookmarkViewModel>(BookmarkViewModel::class.java), ViewHolderProviders<Photo, PhotoViewHolder> {

    override val layoutResource: Int
        get() = R.layout.fragment_bookmarks

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindAction()
        updateList(view, viewModel.photos)
        settingSelectionBarView()
    }

    @SuppressLint("SetTextI18n")
    private fun settingSelectionBarView() {
        selectionToolbar.translationY = -resources.getDimension(R.dimen.actionbarSize)
        selectionToolbar.translationZ = 0f
        actionButton.text = "Delete from Bookmarks"
        val icon = ContextCompat.getDrawable(context!!, R.drawable.outline_delete_outline_24px)
        actionButton.setCompoundDrawablesWithIntrinsicBounds(null, null, icon, null)
    }

    private fun bindAction() {
        viewModel.onItemSelectionChange = { _, itemView, isSelected, totalCount -> onItemSelectionChange(itemView, isSelected, totalCount) }
        viewModel.displayDetail = this::onItemClick
        cancelButton.setOnClickListener(viewModel.onCancelSelectionClick)
        viewModel.notifyDataSetChange = {
            animationSelectionBar(false)
            rvBookmark.adapter?.notifyDataSetChanged()
        }

        actionButton.setOnClickListener(viewModel.onDeleteActionClick)
    }

    private fun onItemClick(photo: Photo, itemView: View) {
        val intent = Intent(context, PhotoDetailActivity::class.java)
        intent.putExtra("DATA", photo)
        val option = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, itemView.itemImage,
                itemView.itemImage.transitionName)
        this.startActivityForResult(intent, 202, option.toBundle())
    }


    private fun onItemSelectionChange(itemView: View, isSelected: Boolean, totalCount: Int) {
        selectionCountText.text = totalCount.toString()
        if (totalCount == 1)
            animationSelectionBar(true)
        else if (totalCount == 0) animationSelectionBar(false)

        PhotoViewHolder.animationSelectionChange(itemView, isSelected)
    }

    private fun updateList(view: View, photos: List<Photo>) {
        view.rvBookmark.layoutManager = GridLayoutManager(context, 4)
        view.rvBookmark.adapter = RecyclerViewAdapter(photos as ArrayList<Photo>, this)
    }

    private fun animationSelectionBar(isShow: Boolean) {
        val toValue = if (isShow) 0f else -resources.getDimension(R.dimen.actionbarSize)
        val slide = ObjectAnimator.ofFloat(selectionToolbar, View.TRANSLATION_Y, selectionToolbar.translationY, toValue)
        slide.interpolator = AnticipateOvershootInterpolator()
        slide.start()
        selectionToolbar.translationZ = if (isShow) resources.getDimension(R.dimen.toolTransitionZ) else 0f
    }

    override fun onCreateViewHolder(container: ViewGroup, itemType: Int): PhotoViewHolder {
        val itemSize = resources.displayMetrics.widthPixels / 4
        val holder = PhotoViewHolder(layoutInflater.inflate(R.layout.item_photo, container, false))
        holder.itemView.layoutParams.apply {
            this.height = itemSize
            this.width = itemSize
        }
        holder.setOnItemClick(viewModel.onItemClick)
        holder.setOnLongClick(viewModel.onItemLongClick)
        return holder
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 202 && resultCode == Activity.RESULT_OK) {
            val photo = data?.getSerializableExtra(PhotoDetailActivity.EXTRA_RESULT) as Photo?
            photo?.let {
                if (!photo.isBookmark) {
                    var removePosition: Int
                    viewModel.photos.let { listItem ->
                        val items = listItem.filter { item -> item.id == photo.id }
                        if (items.isNotEmpty()) {
                            removePosition = listItem.indexOf(items[0])
                            listItem.removeAt(removePosition)
                            rvBookmark.adapter?.notifyItemRemoved(removePosition)
                        }
                    }
                }
            }
        }
    }
}