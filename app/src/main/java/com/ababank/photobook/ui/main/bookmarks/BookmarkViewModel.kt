package com.ababank.photobook.ui.main.bookmarks

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.ababank.photobook.base.BaseViewModel
import com.ababank.photobook.di.component.AppComponent
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.util.CachedUtils
import javax.inject.Inject

/**
 * @author Rithy
 * created at 5/18/2019
 */

class BookmarkViewModel : BaseViewModel() {

    @Inject
    lateinit var cachedUtils: CachedUtils

    var photos: ArrayList<Photo> = arrayListOf()
    var onPhotoChange: ((Int, Int) -> Unit)? = null
    var onItemChange: ((Int) -> Unit)? = null
    var displayDetail: ((Photo, View) -> Unit)? = null
    var onItemSelectionChange: ((Int, View, Boolean, Int) -> Unit)? = null
    var notifyDataSetChange: (() -> Unit)? = null
    var toastMessage: ((String) -> Unit)? = null

    private var isSelectionMode = false

    val onCancelSelectionClick = View.OnClickListener {
        isSelectionMode = false
        photos.let { items ->
            items.forEach { it.isSelected = false }
            notifyDataSetChange?.invoke()
        }
    }

    val onItemClick: (Photo, View) -> Unit = { item, itemView ->
        if (isSelectionMode) {
            val isChecked = item.isSelected
            item.isSelected = !isChecked
            photos.let {
                val totalCount = it.count { photo -> photo.isSelected }
                onItemSelectionChange?.invoke(it.indexOf(item), itemView, item.isSelected, totalCount)
                isSelectionMode = it.any { photo -> photo.isSelected }
            }
        } else {
            displayDetail?.invoke(item, itemView)
        }
    }

    val onItemLongClick: (Photo, View) -> Unit = { item, itemView ->
        isSelectionMode = true
        item.isSelected = true
        photos.let {
            onItemSelectionChange?.invoke(it.indexOf(item), itemView, item.isSelected, it.count { photo -> photo.isSelected })
        }
    }

    val onDeleteActionClick = View.OnClickListener {
        val toDeleteItems = photos.filter { it.isSelected }
        photos.removeAll(toDeleteItems)
        toDeleteItems.forEach {
            it.isSelected= false
            cachedUtils.deleteBookmark(it)
        }

        photos.forEach { it.isSelected = false }
        notifyDataSetChange?.invoke()
    }

    override fun onAttach(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        photos.clear()
        photos.addAll(cachedUtils.getBookmarkPhotos())
    }
}