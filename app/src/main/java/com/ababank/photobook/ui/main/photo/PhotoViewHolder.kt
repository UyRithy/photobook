package com.ababank.photobook.ui.main.photo

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import com.ababank.photobook.commons.BaseViewHolder
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.util.ImageUtils
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_photo.view.*

/**
 * @author Rithy
 * created at 5/18/2019
 */
class PhotoViewHolder(view: View) : BaseViewHolder<Photo>(view) {

    private var _onItemClick: ((Photo, View) -> Unit)? = null
    private var _onLongClick: ((Photo, View) -> Unit)? = null

    fun setOnItemClick(callback: (Photo, View) -> Unit) {
        this._onItemClick = callback
    }

    fun setOnLongClick(callback: (Photo, View) -> Unit) {
        this._onLongClick = callback
    }

    override fun onBind(item: Photo) {
        super.onBind(item)
        itemView.setOnClickListener(onItemClick)
        loadingImage(item)
        bindSelectionMode(item)
    }

    private fun bindSelectionMode(item: Photo) {
        val scale = if (item.isSelected) 0.8f else 1f
        itemView.checkedImage.scaleX= if(item.isSelected) 1f else 0f
        itemView.checkedImage.scaleY= if(item.isSelected) 1f else 0f
        itemView.itemImage.scaleX = scale
        itemView.itemImage.scaleY = scale
    }

    private fun loadingImage(item: Photo) {
        val scale = ImageUtils.PREVIEW_IMAGE_SCALE
        val width = (item.width * scale).toInt()
        val height = (item.height * scale).toInt()
        val url = "https://picsum.photos/id/" + item.id + "/$width/$height"
        itemView.bookmarkImage.visibility = if (item.isBookmark && !item.isSelected) View.VISIBLE else View.GONE
        itemView.setOnLongClickListener(onItemLongClick)
        Glide.with(itemView)
                .load(url)
                .into(itemView.itemImage)
    }

    private val onItemClick = View.OnClickListener {
        item?.let { photo -> _onItemClick?.invoke(photo, itemView) }
    }

    private val onItemLongClick = View.OnLongClickListener {
        item?.let { _onLongClick?.invoke(it, itemView)  }
        true
    }

    companion object {
        fun animationSelectionChange(itemView: View, isSelected: Boolean) {
            val scale = if(isSelected) 0.8f else 1f
            val checkScale = if(isSelected) 1f else 0f
            val animationSet = AnimatorSet()
            val scaleX = ObjectAnimator.ofFloat(itemView.itemImage, View.SCALE_X, itemView.itemImage.scaleX, scale)
            val scaleY = ObjectAnimator.ofFloat(itemView.itemImage, View.SCALE_Y, itemView.itemImage.scaleX, scale)
            val checkScaleX = ObjectAnimator.ofFloat(itemView.checkedImage, View.SCALE_X, itemView.itemImage.scaleX, checkScale)
            val checkScaleY = ObjectAnimator.ofFloat(itemView.checkedImage, View.SCALE_Y, itemView.itemImage.scaleY, checkScale)
            animationSet.interpolator = AnticipateOvershootInterpolator()
            animationSet.playTogether(scaleX, scaleY, checkScaleY, checkScaleX)
            animationSet.start()
        }
    }
}