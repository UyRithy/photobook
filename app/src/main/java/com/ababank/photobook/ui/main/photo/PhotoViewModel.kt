package com.ababank.photobook.ui.main.photo

import android.annotation.SuppressLint
import android.view.View
import com.ababank.photobook.base.BaseViewModel
import com.ababank.photobook.di.component.AppComponent
import com.ababank.photobook.services.AppServices
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.util.CachedUtils
import com.ababank.photobook.util.ConnectionUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Rithy
 * created at 5/18/2019
 */
class PhotoViewModel : BaseViewModel() {

    @Inject
    lateinit var appServices: AppServices

    @Inject
    lateinit var connectionUtils: ConnectionUtils

    @Inject
    lateinit var cachedUtils: CachedUtils

    private var isLoading = false
    private var isSelectionMode: Boolean = false

    private var page: Int = 1
    val photos: ArrayList<Photo> = arrayListOf()
    var onPhotoChange: ((Int, Int) -> Unit)? = null
    var loadingStateChange: ((Boolean) -> Unit)? = null
    var onItemChange: ((Int) -> Unit)? = null
    var displayDetail: ((Photo, View) -> Unit)? = null
    var onItemSelectionChange: ((Int, View, Boolean, Int) -> Unit)? = null
    var notifyDataSetChange: (() -> Unit)? = null
    var toastMessage: ((String) -> Unit)? = null

    val onListEndReach: () -> Unit = {
        if (!isLoading) loadPhotos()
    }

    val onAddToBookmarksClick = View.OnClickListener {
        val selectedItems = photos.filter { it.isSelected }
        photos.forEach { it.isSelected = false }
        selectedItems.forEach {
            it.isBookmark = true
            cachedUtils.saveOrUpdateBookmarkItems(it)
        }
        isSelectionMode = false
        notifyDataSetChange?.invoke()
        toastMessage?.invoke("${selectedItems.size} is added to bookmarks list")
    }

    val onCancelSelectionClick = View.OnClickListener {
        photos.forEach { it.isSelected = false }
        notifyDataSetChange?.invoke()
    }

    val onLongClick: (Photo, View) -> Unit = { photo, itemView ->
        photo.isSelected = true
        isSelectionMode = true
        onItemSelectionChange?.invoke(photos.indexOf(photo), itemView, true, photos.count { it.isSelected })
    }

    val onItemClick: (Photo, View) -> Unit = { photo, itemView ->
        if (!isSelectionMode) displayDetail?.invoke(photo, itemView)
        else {
            val checked = photo.isSelected
            photo.isSelected = !checked
            onItemSelectionChange?.invoke(photos.indexOf(photo), itemView, photo.isSelected, photos.count { it.isSelected })
            isSelectionMode = photos.any { it.isSelected }
        }
    }

    override fun onAttach(appComponent: AppComponent) {
        appComponent.inject(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (!isLoading) {
            if (photos.isNotEmpty()) {
                isLoading = false
                return
            }

            loadPhotos()
        }
    }

    @SuppressLint("CheckResult")
    private fun loadPhotos() {
        isLoading = true
        loadingStateChange?.invoke(isLoading)
        appServices.fetchPhotos(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnComplete { loadingStateChange?.invoke(false) }
                .subscribe(this::onLoadSuccess) { handelError(it) }
    }

    private fun onLoadSuccess(response: List<Photo>) {
        isLoading = false
        if (response.isNotEmpty()) {
            cachedUtils.savePhotos(response, page)
            page++
            response.forEach { it.isBookmark = cachedUtils.isBookmark(it.id) }
            this.photos.addAll(response)
            val startedItem = response[0]
            onPhotoChange?.invoke(photos.indexOf(startedItem), photos.size)
        }
    }

    fun onReceivedResult(photo: Photo) {
        val items = photos.filter { it.id == photo.id }
        if (items.isNotEmpty()) {
            val item = items[0]
            if (item.isBookmark != photo.isBookmark) {
                item.isBookmark = photo.isBookmark
                onItemChange?.invoke(photos.indexOf(item))
            }
        }
    }

    private fun handelError(error: Throwable) {
        isLoading = false
        loadingStateChange?.invoke(isLoading)
        error.printStackTrace()
        if (!connectionUtils.isNetworkAvailable()) {
            val response = cachedUtils.getPhotoCachedByPage(page)
            response?.let { onLoadSuccess(response) }
        }
    }
}