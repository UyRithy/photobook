package com.ababank.photobook.ui.main.photo

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.ababank.photobook.R
import com.ababank.photobook.base.BaseFragment
import com.ababank.photobook.commons.RecyclerViewAdapter
import com.ababank.photobook.commons.ViewHolderProviders
import com.ababank.photobook.commons.extention.onEndReached
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.ui.main.MainHost
import com.ababank.photobook.ui.main.photo.detail.PhotoDetailActivity
import kotlinx.android.synthetic.main.fragment_photos.*
import kotlinx.android.synthetic.main.fragment_photos.view.*
import kotlinx.android.synthetic.main.item_photo.view.*
import kotlinx.android.synthetic.main.selection_tool_bar.*

/**
 * @author Rithy
 * created at 5/18/2019
 */
class PhotosFragment : BaseFragment<PhotoViewModel>(PhotoViewModel::class.java), ViewHolderProviders<Photo, PhotoViewHolder> {

    companion object {
        fun newInstance(host: MainHost): PhotosFragment {
            val instance = PhotosFragment()
            instance.mainHost = host
            return instance
        }
    }

    private var gridSpan: Int = 4
    private var mainHost: MainHost? = null
    override val layoutResource: Int get() = R.layout.fragment_photos

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindView(view)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        newConfig?.let {
            gridSpan = if (it.orientation == Configuration.ORIENTATION_LANDSCAPE) 6 else 4
            (rvPhotos.layoutManager as GridLayoutManager).spanCount = gridSpan
            rvPhotos.adapter = RecyclerViewAdapter(viewModel.photos, this)
        }
    }

    private fun bindView(view: View) {
        selectionToolbar.translationY = -resources.getDimension(R.dimen.actionbarSize)
        selectionToolbar.translationZ = 0f
        view.rvPhotos.layoutManager = GridLayoutManager(context, gridSpan)
        view.rvPhotos.adapter = RecyclerViewAdapter(viewModel.photos, this)
        view.rvPhotos.onEndReached(viewModel.onListEndReach)
        view.spin_center.visibility = if (viewModel.photos.isEmpty()) View.VISIBLE else View.GONE
        cancelButton.setOnClickListener(viewModel.onCancelSelectionClick)
        actionButton.setOnClickListener(viewModel.onAddToBookmarksClick)
        viewModel.onPhotoChange = { startIndex, itemCount -> view.rvPhotos.adapter?.notifyItemRangeInserted(startIndex, itemCount) }
        viewModel.loadingStateChange = { isLoading ->
            view.spin_center.visibility = if (isLoading && viewModel.photos.isEmpty()) View.VISIBLE else View.GONE
            view.spin_kit.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        viewModel.onItemChange = { position -> view.rvPhotos.adapter?.notifyItemChanged(position) }
        viewModel.displayDetail = this::onItemClick
        viewModel.onItemSelectionChange = { _, itemView, isSelected, count -> onItemSelectionChange(itemView, isSelected, count) }
        viewModel.notifyDataSetChange = {
            rvPhotos.adapter?.notifyDataSetChanged()
            animationSelectionBar(false)
        }
        viewModel.toastMessage = { message -> Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
    }

    private fun animationSelectionBar(isShow: Boolean) {
        val toValue = if (isShow) 0f else -resources.getDimension(R.dimen.actionbarSize)
        val slide = ObjectAnimator.ofFloat(selectionToolbar, View.TRANSLATION_Y, selectionToolbar.translationY, toValue)
        slide.interpolator = AnticipateOvershootInterpolator()
        slide.start()
        selectionToolbar.translationZ = if (isShow) resources.getDimension(R.dimen.toolTransitionZ) else 0f
    }

    override fun onCreateViewHolder(container: ViewGroup, itemType: Int): PhotoViewHolder {
        val holder = PhotoViewHolder(layoutInflater.inflate(R.layout.item_photo, container, false))
        val imageSize = resources.displayMetrics.widthPixels / gridSpan
        holder.itemView.apply {
            layoutParams.width = imageSize
            layoutParams.height = imageSize
            this.itemImage.clipToOutline = true
        }
        holder.setOnItemClick(viewModel.onItemClick)
        holder.setOnLongClick(viewModel.onLongClick)
        return holder
    }

    private fun onItemSelectionChange(itemView: View, isSelected: Boolean, count: Int) {

        selectionCountText.text = count.toString()
        PhotoViewHolder.animationSelectionChange(itemView, isSelected)
        if (count == 1)
            animationSelectionBar(true)
        else if (count == 0) animationSelectionBar(false)
    }

    private fun onItemClick(photo: Photo, itemView: View) {
        val intent = Intent(context, PhotoDetailActivity::class.java)
        intent.putExtra("DATA", photo)
        val option = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, itemView.itemImage, itemView.itemImage.transitionName)
        this.startActivityForResult(intent, 202, option.toBundle())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 202 && resultCode == Activity.RESULT_OK) {
            val photo = data?.getSerializableExtra(PhotoDetailActivity.EXTRA_RESULT) as Photo?
            photo?.let { viewModel.onReceivedResult(it) }
        }
    }
}