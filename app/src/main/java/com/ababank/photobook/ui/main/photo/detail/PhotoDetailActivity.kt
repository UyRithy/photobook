package com.ababank.photobook.ui.main.photo.detail

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.ababank.photobook.R
import com.ababank.photobook.base.BaseActivity
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.util.ImageUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_photo_detail.*


/**
 * @author Rithy
 * created at 5/18/2019
 */

class PhotoDetailActivity : BaseActivity<PhotoDetailViewModel>(PhotoDetailViewModel::class.java) {

    companion object {
        const val EXTRA_RESULT = "PHOTO"
    }

    override val layoutResource: Int get() = R.layout.activity_photo_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        imageBack.setOnClickListener { onBackPressed() }
        viewModel.photo.observe(this, Observer { photo -> loadPreview(photo) })
        viewModel.bookMarkStateChange = {
            bookmarksButton.setImageResource(if (it)
                R.drawable.ic_baseline_bookmarks_24px
            else
                R.drawable.ic_outline_bookmarks_24px)
        }

        viewModel.attachIntent(intent)
        bookmarksButton.setOnClickListener(viewModel.onBookmarkClick)
        shareButton.setOnClickListener(onShareClick)
    }

    private val onShareClick = View.OnClickListener {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_TEXT, viewModel.photo.value!!.url)
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }

    @SuppressLint("CheckResult")
    private fun loadPreview(item: Photo) {
        val scale = ImageUtils.PREVIEW_IMAGE_SCALE
        val width = (item.width * scale).toInt()
        val height = (item.height * scale).toInt()
        val url = "https://picsum.photos/id/" + item.id + "/$width/$height"
        Glide.with(image)
                .load(url).listener(object: RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        progressbar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        Handler().postDelayed({ displayPhoto(item, resource) }, 200)
                        return true
                    }
                })
                .into(image)
    }

    private val requestListener = object : RequestListener<Drawable> {
        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
            progressbar.visibility = View.GONE
            Toast.makeText(this@PhotoDetailActivity, "Loading full size failed", Toast.LENGTH_LONG).show()
            return false
        }

        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
            progressbar.visibility = View.GONE
            image.setImageDrawable(resource)
            return true
        }
    }

    @SuppressLint("CheckResult")
    private fun displayPhoto(item: Photo, preview: Drawable?) {
        val scale = ImageUtils.IMAGE_SCALE
        val width = (item.width * scale).toInt()
        val height = (item.height * scale).toInt()
        val url = "https://picsum.photos/id/" + item.id + "/$width/$height"

        Glide.with(this)
                .load(url)
                .placeholder(preview)
                .listener(requestListener)
                .into(image)
    }

    override fun onBackPressed() {
        viewModel.photo.value?.let {
            val data = Intent()
            data.putExtra(EXTRA_RESULT, it)
            setResult(Activity.RESULT_OK, data)
        }
        super.onBackPressed()
    }
}
