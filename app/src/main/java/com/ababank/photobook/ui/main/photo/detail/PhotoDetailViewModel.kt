package com.ababank.photobook.ui.main.photo.detail

import android.content.Intent
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.ababank.photobook.base.BaseViewModel
import com.ababank.photobook.di.component.AppComponent
import com.ababank.photobook.services.response.model.Photo
import com.ababank.photobook.util.CachedUtils
import javax.inject.Inject

/**
 * @author Rithy
 * created at 5/18/2019
 */

class PhotoDetailViewModel : BaseViewModel() {

    @Inject
    lateinit var cachedUtils: CachedUtils
    val photo = MutableLiveData<Photo>()
    var bookMarkStateChange: ((Boolean) -> Unit)? = null

    val onBookmarkClick = View.OnClickListener {
        photo.value?.let {
            it.isBookmark = !it.isBookmark
            bookMarkStateChange?.invoke(it.isBookmark)
            if (it.isBookmark)
                cachedUtils.saveOrUpdateBookmarkItems(it)
            else cachedUtils.deleteBookmark(it)
        }
    }

    fun attachIntent(intent: Intent) {
        val data = intent.getSerializableExtra("DATA")
        data?.let {
            if (it is Photo) {
                photo.value = it
                it.isBookmark =cachedUtils.isBookmark(it.id)
                bookMarkStateChange?.invoke(it.isBookmark)
            }
        }
    }

    override fun onAttach(appComponent: AppComponent) {
        appComponent.inject(this)
    }
}