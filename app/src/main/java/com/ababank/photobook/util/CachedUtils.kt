package com.ababank.photobook.util


import android.content.Context
import android.util.Log
import com.ababank.photobook.MyApplication
import com.ababank.photobook.services.response.model.Photo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Singleton
class CachedUtils @Inject constructor(private val context: MyApplication) {

    companion object {
        const val PERSISTENT_NAME = "CACHED"
        const val PHOTOS_KEY = "PHOTOS"
        const val BOOKMARK_KEY = "BOOKMARK_PHOTO"
    }

    private var bookmarks: ArrayList<Photo>? = null
    private var cachedPhotos: HashMap<Int, List<Photo>>? = null
    private val preference by lazy { context.getSharedPreferences(PERSISTENT_NAME, Context.MODE_PRIVATE) }

    fun saveOrUpdateBookmarkItems(photo: Photo) {
        val bookmarkItems = getBookmarkPhotos() as ArrayList<Photo>
        val editor = preference.edit()
        val isExist = bookmarkItems.any { it.id == photo.id }
        if (isExist) {
            bookmarkItems.filter { it.id == photo.id }.forEach { it.isBookmark = photo.isBookmark }
        } else bookmarkItems.add(photo)
        val json = Gson().toJson(bookmarkItems)
        editor.putString(BOOKMARK_KEY, json)
        editor.apply()
    }

    fun deleteBookmark(photo: Photo) {
        val bookmarkItems = getBookmarkPhotos() as ArrayList<Photo>
        val editor = preference.edit()
        val toDelete = bookmarkItems.filter { it.id == photo.id }
        bookmarkItems.removeAll(toDelete)
        val json = Gson().toJson(bookmarkItems)
        editor.putString(BOOKMARK_KEY, json)
        editor.apply()
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun getBookmarkPhotos(): List<Photo> {
        if (this.bookmarks == null) {
            val jsonPhotos = preference.getString(BOOKMARK_KEY, "[]")
            Log.d(">>>>", jsonPhotos)
            val listType = object : TypeToken<List<Photo>>() {}.type
            this.bookmarks = Gson().fromJson(jsonPhotos, listType)
        }

        return this.bookmarks!!
    }
    
    fun savePhotos(photos: List<Photo>, page: Int) {
        try {
            val itemsCached = getCachedPhoto()
            val editor = preference.edit()
            itemsCached[page] = photos
            val json = Gson().toJson(itemsCached)
            Log.d("SAVE>>>>", json)
            editor.putString(PHOTOS_KEY, json)
            editor.apply()
            Log.d(">>>>", "Save")

        }catch (e: Exception) {
            e.printStackTrace()
            Log.e(">>>>", e.message)
        }
    }

    fun getPhotoCachedByPage(page: Int) : List<Photo>? {
        return getCachedPhoto()[page]
    }

    private fun getCachedPhoto(): HashMap<Int, List<Photo>> {
        if (cachedPhotos == null) {
            val json = preference.getString(PHOTOS_KEY, "{}")
            Log.d("GET_CACHED>>>>", json)
            val type = object: TypeToken<HashMap<Int, List<Photo>>>() {}.type
            this.cachedPhotos = Gson().fromJson(json, type)
        }
        
        return cachedPhotos!!
    }

    fun isBookmark(id: Int): Boolean {
        return getBookmarkPhotos().any { it.id == id }
    }
}