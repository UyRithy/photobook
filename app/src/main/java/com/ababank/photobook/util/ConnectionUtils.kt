package com.ababank.photobook.util

import android.content.Context
import android.net.ConnectivityManager
import com.ababank.photobook.MyApplication
/**
 * @author Rithy
 * created at 5/21/2019
 */

class ConnectionUtils(val app: MyApplication) {

    fun isNetworkAvailable(): Boolean {
        val connectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        val activeNetworkInfo = connectivityManager!!.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}