package com.ababank.photobook.util
/**
 * @author Rithy
 * created at 5/18/2019
 */
object ImageUtils {

    //load image preview setting
    const val PREVIEW_IMAGE_SCALE = 0.2

    //load detail image
    const val IMAGE_SCALE = 0.8
}